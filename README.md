# django_test

A basic forum style website built with django following the [simple is better tutorial series here](https://simpleisbetterthancomplex.com/series/2017/09/04/a-complete-beginners-guide-to-django-part-1.html).


## How to start project

## Environment Keys
Env file with secret key on onedrive here:
https://1drv.ms/u/s!Aj_uXHFXJCwolIYfIO7f-1_PGsYkyQ?e=FxXHu8

#### Setup

Project uses **virtualenv** for the python virtual environment. 
First time pulling down repo requires creating a virtual env. Make sure you have ```virtualenv``` 
in your native python 3x installation:

```pip install virtualenv```

Then do:

```virtualenv venv``` 


#### Run Project:
Activate Virtual Environment:
 
```venv\Scripts\activate```

```which python``` should now point to the path of your virtual environment.

Start up django server:
```python manage.py runserver```

Run any tests:
```python manage.py test --verbosity=2 boards.tests```

#### Resources:

[Django-admin Ref](https://docs.djangoproject.com/en/2.2/ref/django-admin/)

##### Notes to self:

Configuration files, password info, deployment configs and other sensitive deploy info
is located on OneDrive.