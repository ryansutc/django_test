## How to Deploy with Docker

#### Generate Personal Access Token

https://cloud.digitalocean.com/account/api/tokens?i=50e0e7

#### Create the Digital Ocean Droplet

```
docker-machine create --driver digitalocean --digitalocean-access-token xxxxx ubuntu-django-droplet
:REM: ensure machine running!
docker-machine ls 

docker run -d -p 8000:80 --name webserver yerapp?

docker-compose up
```