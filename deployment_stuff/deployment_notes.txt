ssh-keygen
passphrase: icebox


--
new ubuntu server
ip address: 165.22.232.6
root pw: LearnDjango123

--- 
in order to get pip for python3.6,
needed to install distutils:

sudo apt-get install python3-distutils

--
created user boards w. password: boards123
this is the application owner. Django code lies in his home dir.

u_boards password = 'AABBc3p0123HA'

// ran into issues getting postgres driver via:
pip install psycopg2
// Error: b'You need to install postgresql-server-dev-X.Y for building a server-side extension or libpq-dev for building a client-side application.\n
//tried: https://stackoverflow.com/questions/28253681/you-need-to-install-postgresql-server-dev-x-y-for-building-a-server-side-extensi

Still not working. throwing error command 'x86_64-linux-gnu-gc no such file
the trick seems to be to install python3-dev as per [here](https://stackoverflow.com/questions/5420789/how-to-install-psycopg2-with-pip-on-python) and explicitly stating latest version
as per [here](https://www.dark-hamster.com/web/programming-web/python-programming-web/how-to-solve-error-on-installing-psycopg2-using-pip-by-adding-specific-version/):
// error with pscp transfer files
// pscp source_file_name userid@server_name:/path/destination_file_name.
// could not handle public key. Tried reinstalling putty, was an issue with config.


//for putty have to make specify special private key.
// this works: 

pscp -i "C:\Users\RSutcliffe\.ssh\id_putty_ecdsa.ppk" "C:\Users\RSutcliffe\Desktop\django_test\django_test\.env_prod" root@165.22.224.188:/home/boards/django_test/django_test/.env

//had to manually add python-decouple, dj_database_url. Wasn't in requirements.txt file.

Followed guidance here for the postgres driver psycopg and explicitly
installed version 2.7.4 (https://www.dark-hamster.com/web/programming-web/python-programming-web/how-to-solve-error-on-installing-psycopg2-using-pip-by-adding-specific-version/)


Need to activate python instance:

Transfer your nginx config file:
pscp -i "C:\Users\RSutcliffe\.ssh\id_putty_ecdsa.ppk" "C:\Users\RSutcliffe\Desktop\django_test\nginx_config_stuff\boards.conf" root@165.22.224.188:/etc/nginx/sites-available/boards

Transfer gunicorn file:
pscp -i "C:\Users\RSutcliffe\.ssh\id_putty_ecdsa.ppk" "C:\Users\RSutcliffe\Desktop\django_test\gunicorn_start" root@165.22.224.188:/home/boards/gunicorn_start