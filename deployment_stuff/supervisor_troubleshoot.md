## Problem: Supervisor cannot start subprocess

I have a django app on a Ubuntu server run with gunicorn and started/stopped with [Supervisor](http://supervisord.org/running.html)

[how to use Django with gunicorn](https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/gunicorn/)

the supervisor files are located here:
```
/etc/supervisor/conf.d/boards.conf
/etc/supervisor/conf.d/cat.conf
```

```
sudo supervisorctl reread
sudo supervisorctl update
sudo supervisorctl status boards
```

## What I tried:
Looked at supervisor logs:
```cat /var/log/supervisor/supervisord.log```

Log reports:
```
2019-08-05 18:19:53,449 INFO spawned: 'cat' with pid 2709
2019-08-05 18:19:53,464 INFO exited: cat (exit status 127; not expected)
2019-08-05 18:19:54,468 INFO spawned: 'cat' with pid 2710
2019-08-05 18:19:54,480 INFO exited: cat (exit status 127; not expected)
2019-08-05 18:19:56,486 INFO spawned: 'cat' with pid 2711
2019-08-05 18:19:56,498 INFO exited: cat (exit status 127; not expected)
2019-08-05 18:19:59,504 INFO spawned: 'cat' with pid 2712
2019-08-05 18:19:59,517 INFO exited: cat (exit status 127; not expected)
2019-08-05 18:20:00,518 INFO gave up: cat entered FATAL state, too many start retries too quickly
```
cleared logs:
```
truncate -s 0 /var/log/supervisor/supervisord.log
```

Notes: Code 127 indicates a 'command not found error' ([source](https://www.linuxquestions.org/questions/linux-general-1/exit-status-127-a-462787/). Suggests cat file has typos or syntax errors?

- switched permissions to ```cattest``` via:

	```
	sudo chmod 754 cattest
	```

Had to [switch code from dos to unix return chars](https://stackoverflow.com/questions/14219092/bash-my-script-bin-bashm-bad-interpreter-no-such-file-or-directory)

```
dos2unix catttest
```

how to upload files to remote system w. Putty:
pscp -i "C:\Users\RSutcliffe\.ssh\id_putty_ecdsa.ppk" "C:\Users\RSutcliffe\Desktop\django_test\django_test\.env_prod" root@165.22.224.188:/home/boards/django_test/django_test/.env

cd /home/boards
venv/bin/activate
source gunicorn_start


## Configuring NGINX

pscp -i "C:\Users\RSutcliffe\.ssh\id_putty_ecdsa.ppk" "C:\Users\RSutcliffe\Desktop\django_test\nginx_config_stuff\boards.conf" root@165.22.224.188:/etc/nginx/sites-available/boards
```
sudo service nginx restart
```

log file:
/home/boards/logs/nginx-error.log

## Gunicorn Troubleshooting:

#### Issue
starting nginx: ```sudo systemctl start nginx```
stopping nginx: ```sudo systemctl stop nginx```
restart nginx: ```sudo systemctl restart nginx```

```sudo systemctl status nginx```


2019/08/11 02:56:36 [crit] 2413#2413: *1 connect() to unix:/home/boards/run/gunicorn.sock failed (2: No such file or directory) while connecting to upstream, client: 69.172.188.137, server: www.rsviewer.xyz, request: "GET / HTTP/1.1", upstream: "http://unix:/home/boards/run/gunicorn.sock:/", host: "165.22.224.188"
2019/08/11 02:56:36 [crit] 2413#2413: *3 connect() to unix:/home/boards/run/gunicorn.sock failed (2: No such file or directory) while connecting to upstream, client: 69.172.188.137, server: www.rsviewer.xyz, request: "GET /favicon.ico HTTP/1.1", upstream: "http://unix:/home/boards/run/gunicorn.sock:/favicon.ico", host: "165.22.224.188"
2019/08/11 02:56:54 [crit] 2413#2413: *5 connect() to unix:/home/boards/run/gunicorn.sock failed (2: No such file or directory) while connecting to upstream, client: 69.172.188.137, server: www.rsviewer.xyz, request: "GET /django_test HTTP/1.1", upstream: "http://unix:/home/boards/run/gunicorn.sock:/django_test", host: "165.22.224.188"
2019/08/11 03:33:00 [crit] 2413#2413: *7 connect() to unix:/home/boards/run/gunicorn.sock failed (2: No such file or directory) while connecting to upstream, client: 69.172.188.137, server: www.rsviewer.xyz, request: "GET /django_test HTTP/1.1", upstream: "http://unix:/home/boards/run/gunicorn.sock:/django_test", host: "165.22.224.188"
2019/08/11 03:33:01 [crit] 2413#2413: *7 connect() to unix:/home/boards/run/gunicorn.sock failed (2: No such file or directory) while connecting to upstream, client: 69.172.188.137, server: www.rsviewer.xyz, request: "GET /favicon.ico HTTP/1.1", upstream: "http://unix:/home/boards/run/gunicorn.sock:/favicon.ico", host: "165.22.224.188"

### Trying to just vanilla run gunicorn:
su boards
cd /home/boards
cd django_test/django_test/
exec ../../venv/bin/gunicorn django_test.settings:application
--bind=unix:/home/boards/run/gunicorn.sock

get a list of all open sockets on machine:
```
netstat -a -p --unix
```
./etc/supervisor/supervisord.conf 
/var/run/supervisor.sock

pscp -i "C:\Users\RSutcliffe\.ssh\id_putty_ecdsa.ppk" "C:\Users\RSutcliffe\Desktop\django_test\supervisord.conf" root@165.22.224.188:/etc/supervisor/supervisor.conf


this worked to connect to my database with PgAdmin:
https://www.digitalocean.com/community/questions/how-do-you-remote-connect-with-pgadmin-4-to-a-postgresql-on-a-droplet