
from django.test import TestCase
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse
from django.urls import resolve
from .views import signup

# Create your tests here.
class SignUpTests(TestCase):
    def setUp(self):
        url = reverse('signup')
        self.response = self.client.get(url)

    def test_signup_status_code(self):
        self.assertEquals(self.response.status_code, 200)

class SuccessfulSignUpTests(TestCase):
    def setUp(self):
        url = reverse('signup')
        data = {
            'username': 'john',
            'password1': 'abcdef123456',
            'password2': 'abcdef123456'
        }
        self.response = self.client.post(url, data)
        self.home_url = reverse('home')


    def test_redirection(self):
        '''
        A valid form submission should redirect user to the home page
        :return:
        '''
        self.assertRedirects(self.response, self.home_url)

    def test_user_creation(self):
        self.assertTrue(User.objects.exists())


class PasswordResetMailTests
    '''
    These are a series of tests to make sure 
    password resets look to be working
    '''

    def setUp(self):
        User.objects.create_user(username='john', email='john@doe.com', password='123')
        self.response = self.client.post(reverse('password_reset'), { 'email': 'john@doe.com'})
        self.email = mail.outbox[0]


    def test_email_subject(self):
        self.assertEqual('[Django Boards] Please reset your password', self.email.subject)


    def test_email_body(self):
        context = self.response.context
        token =context.get('token')
        uid = context.get('uid')
        password_reset_token_url = reverse('password_reset_confirm', kwards={
            'uidb64': uid,
            'token': token
        })
        self.assertIn(password_reset_token_url, self.email.body)
        self.assertIn('john', self.email.body)
        # etc..


