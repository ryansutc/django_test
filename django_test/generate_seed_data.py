'''
A series of functions to seed/refresh the database with test data
'''

from django.contrib.auth.models import User
from django_test.boards.models import Board, Topic, Post

def seed_data():
    user = User.objects.first()

    board = Board.objects.get(name='Django')

    for i in range(100):
        subject = 'Topic test #{}'.format(i)
        topic = Topic.objects.create(subject=subject, board=board, starter=user)
        Post.objects.create(message='Lorem ipsum...', topic=topic, created_by=user)


seed_data()