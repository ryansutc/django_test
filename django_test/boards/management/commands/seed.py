# <project>/<app>/management/commands/seed.py
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from boards.models import Board, Topic, Post
import random

# python manage.py seed --mode=refresh

""" Clear all data and creates addresses """
MODE_REFRESH = 'refresh'

""" Clear all data and do not create any object """
MODE_CLEAR = 'clear'

class Command(BaseCommand):
    help = "seed database for testing and development."

    def add_arguments(self, parser):
        parser.add_argument('--mode', type=str, help="Mode")

    def handle(self, *args, **options):
        self.stdout.write('seeding data...')
        self.run_seed(options['mode'])
        self.stdout.write('done.')

    def run_seed(self, mode):
        """ Seed database based on mode

        :param mode: refresh / clear
        :return:
        """
        # Clear data from tables
        # clear_data()
        if mode == MODE_CLEAR:
            return
        else:
            create_random_data()


def clear_data():
    """Deletes all the table data"""
    #logger.info("Delete Address instances")
    Board.objects.all().delete()
    #User.objects.all().delete()

    # lets reinstate the admin user:
    # admin_user = User(name='admin', )



def create_random_data():
    """Creates an address object combining different elements from the list"""
    # create a new user here:

    user = User.objects.first()
    Board.objects.all().delete()
    board = Board(name='Test', description='This is an auto-generated board.')
    board.save()

    board2 = Board(name='Test2', description='This is a second auto-created board.')
    board2.save()
    for i in range(50):
        subject = 'Topic test #{}'.format(i)
        topic = Topic.objects.create(subject=subject, board=board, starter=user)
        Post.objects.create(message='Lorem ipsum...', topic=topic, created_by=user)

    # user = User.objects.filter(username='dave')
    user = User.objects.get(username='dave')
    for i in range(50):
        subject = 'Dummy topic #{}'.format(i)
        topic = Topic.objects.create(subject=subject, board=board2, starter=user)
        Post.objects.create(message='Doo doo doo..', topic=topic, created_by=user)
